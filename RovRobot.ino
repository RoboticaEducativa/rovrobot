#include <SoftwareSerial.h>

/*
  Código para RovRobot Educativo V1
  By: Jhon Jairo Bravo Castro
  Email: yjairobravo@gmail.com

*/

SoftwareSerial miSerial(8, 10); //rx tx


#define PIN_1_MOTOR_A 2
#define PIN_2_MOTOR_A 3

#define PIN_1_MOTOR_B 4
#define PIN_2_MOTOR_B 5

#define PIN_LED_DER 13
#define PIN_LED_IZQ 12

#define PIN_LAMPARA 7

void setup() {
  // put your setup code here, to run once:
  pinMode(PIN_1_MOTOR_A, OUTPUT); //PWM
  pinMode(PIN_2_MOTOR_A, OUTPUT); //PWM

  pinMode(PIN_1_MOTOR_B, OUTPUT); //PWM
  pinMode(PIN_2_MOTOR_B, OUTPUT); //PWM

  pinMode(PIN_LED_DER, OUTPUT);
  pinMode(PIN_LED_IZQ, OUTPUT);

  pinMode(PIN_LAMPARA, OUTPUT);
  miSerial.begin(9600);
  delay(2000);
  stop();
}

void loop() {

  if (miSerial.available() > 0) {
    int data = miSerial.read();


    //Serial.print(data);
   // Serial.println();

    switch (data) {
      case 97: //a
        stop();
        adelante();
        break;
      case 98: //b
        stop();
        atras();
        break;
      case 99: //c
        stop();
        izquierda();
        break;
      case 100: //d
        stop();
        derecha();
        //giroDiagonalDerecho();
        break;
      case 101: //e
        stop();
        break;
      case 104: //h
        stop();
        giroDiagonalIzquierdo();
        break;
      case 105://i
        stop();
        giroDiagonalDerecho();
        break;
      case 106://j
        encenderLampara();
        break;
      case 107://k
        apagarLampara();
        break;    
    }

  }

  delay(20);
}

void encenderLampara(){
  digitalWrite(PIN_LAMPARA,HIGH);
}

void apagarLampara() {
  digitalWrite(PIN_LAMPARA,LOW);
}

void stop() {

  digitalWrite(PIN_1_MOTOR_A, LOW);
  digitalWrite(PIN_2_MOTOR_A, LOW);
  digitalWrite(PIN_1_MOTOR_B, LOW);
  digitalWrite(PIN_2_MOTOR_B, LOW);

  digitalWrite(PIN_LED_DER, LOW);
  digitalWrite(PIN_LED_IZQ, LOW);
}

void giroDiagonalDerecho() {
  digitalWrite(PIN_1_MOTOR_A, LOW);
  digitalWrite(PIN_2_MOTOR_A, HIGH);

  digitalWrite(PIN_2_MOTOR_B, HIGH);
  digitalWrite(PIN_1_MOTOR_B, LOW);
  delay(200); // giro 45 Grados

  digitalWrite(PIN_2_MOTOR_B, LOW);
  digitalWrite(PIN_1_MOTOR_B, HIGH);

   onLedDerecho();
}

void giroDiagonalIzquierdo() {
  digitalWrite(PIN_2_MOTOR_B, LOW);
  digitalWrite(PIN_1_MOTOR_B, HIGH);

  digitalWrite(PIN_1_MOTOR_A, HIGH);
  digitalWrite(PIN_2_MOTOR_A, LOW);

  delay(200); // giro 45 Grados

  digitalWrite(PIN_2_MOTOR_A, HIGH);
  digitalWrite(PIN_1_MOTOR_A, LOW);

  onLedIzquierdo();
}

void derecha() {
  digitalWrite(PIN_1_MOTOR_A, LOW);
  digitalWrite(PIN_2_MOTOR_A, HIGH);
  digitalWrite(PIN_1_MOTOR_B, HIGH);
  digitalWrite(PIN_2_MOTOR_B, LOW);

   onLedDerecho();
}


void izquierda() {
  digitalWrite(PIN_1_MOTOR_A, HIGH);
  digitalWrite(PIN_2_MOTOR_A, LOW);
  digitalWrite(PIN_1_MOTOR_B, LOW);
  digitalWrite(PIN_2_MOTOR_B, HIGH);

onLedIzquierdo();

}
void adelante() {
  digitalWrite(PIN_1_MOTOR_A, LOW); //ADELANTE
  digitalWrite(PIN_2_MOTOR_A, HIGH);
  
  digitalWrite(PIN_1_MOTOR_B, LOW);
  digitalWrite(PIN_2_MOTOR_B, HIGH);
  flashLed();

}

void atras() {

  digitalWrite(PIN_1_MOTOR_A, HIGH); //ATRAS
  digitalWrite(PIN_2_MOTOR_A, LOW);
  digitalWrite(PIN_1_MOTOR_B, HIGH);
  digitalWrite(PIN_2_MOTOR_B, LOW);

}

void flashLed() {
  for (int i = 0; i < 2; i++) {
    digitalWrite(PIN_LED_DER, HIGH);
    digitalWrite(PIN_LED_IZQ, LOW);
    delay(40);
    digitalWrite(PIN_LED_IZQ, HIGH);
    digitalWrite(PIN_LED_DER, LOW);
    delay(40);
  }
  digitalWrite(PIN_LED_DER, HIGH);
}

void onLedDerecho() {
  digitalWrite(PIN_LED_DER, HIGH);
  digitalWrite(PIN_LED_IZQ, LOW);
}

void onLedIzquierdo() {
  digitalWrite(PIN_LED_DER, LOW);
  digitalWrite(PIN_LED_IZQ, HIGH);
}
